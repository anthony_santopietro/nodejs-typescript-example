# Simple NodeJS TypeScript Example project

## Prerequisites

Make sure following is installed:

- NodeJS v6.x
- VS Code v1.8.1

## How to setup for development

Install global dependencies (or verify they exist already):
```
npm install typescript gulp --global
```

To install all the current project dependencies, navigate into project directory and type:
```
npm install
```

## How to build

There are currently two ways to build the project: 

- Command-line: type `gulp build`
- VS Code Task Runner: The task runner build task command is configured to invoke the gulp build task.  To invoke the build task, 
  either press `Ctrl+Shift+B` or use command palette to find and run the build task.

## Debugging

Project is configured for simple debugging within VS Code.  Refer to [https://code.visualstudio.com/docs/editor/node-debugging](https://code.visualstudio.com/docs/editor/node-debugging)
for more information.

1. Open the project folder from within VS Code.
1. Open `server.ts` file and add breakpoint on `res.send('Hello World');` line.
1. Select `Debug` Activity (`Ctrl+Shift+D` by default).
1. Make sure `Launch` configuration is selected, then start debugging (`F5` by default).
1. Start Postman ([https://www.getpostman.com/](https://www.getpostman.com/)) and submit `GET http://localhost:4500/` request.  You should see the breakpoint hit in VS Code.
