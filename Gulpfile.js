var gulp = require('gulp');
var tsc = require('gulp-typescript');
var del = require('del');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var path = require('path');

var options = {
    srcDir: 'src',
    outDir: 'dist'
}

// removes the build output directory
function cleanOutputDirectory() {
    return del(options.outDir);
}

// compiles the sources
function compileSources() {
    var sources = [
        'node_modules/@types/**/*.d.ts',
        options.srcDir + '/**/*.ts'
    ];
    var tsProject = tsc.createProject("tsconfig.json");
    
    var result = gulp.src(sources)
                     .pipe(sourcemaps.init())
                     .pipe(tsProject());

    return result.js
                 .pipe(sourcemaps.write('.', { 
                     sourceRoot: function(file) {
                         return file.cwd + '/' + options.srcDir;
                     }
                  }))
                 .pipe(gulp.dest(options.outDir));
}


// Gulp Tasks
gulp.task('clean', cleanOutputDirectory);
gulp.task('build', ['clean'], compileSources);

gulp.task('default', ['build']);