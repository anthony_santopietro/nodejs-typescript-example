import * as express from "express";
let app: express.Express = express();

app.get('/', (req: express.Request, res: express.Response) => {
    res.send('Hello World');
});

let port: Number = 4500;
app.listen(port, () => {
    console.log('server listening on ' + port);
});
